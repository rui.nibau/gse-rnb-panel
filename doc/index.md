---
title:      gse : rnb-panel
banner:     /lab/gse-rnb-panel/pics/1.0.0.png
date:       2021-04-06
updated:    2024-11-01
cats:       [informatique]
tags:       [gnome-shell, extensions]
techs:      [javascript, gnome]
version:    47.0.0
source:     https://framagit.org/rui.nibau/gse-rnb-panel
issues:     https://framagit.org/rui.nibau/gse-rnb-panel/-/issues
download:   /lab/gse-rnb-panel/build/latest/rnb-panel.zip
itemtype:   SoftwareApplication
pagesassnippets: true
intro:      Gnome shell extension to customize top panel. For Gnome ≥ 45.
status:     in-process
---

## Features

°°pic pos-centre sz-full°°
![Preferences](/lab/gse-rnb-panel/pics/1.0.0-prefs.png)

Apps bar:
    • Display favorite applications.
    • Display running applications.
    • Indicates the number of opened windows for each application.
    • click : open window | minimize/restore window | cycle through windows.
    • righ-click on icon : Application menu.
    • Opens maximized windows in the last empty workspace.
Apps grid:
    • Button to toggle applications grid display.
Scroll:
    • Switch between workspaces by scrolling on the main panel.
    • Focus on the window of the current workspace.
Transparency:
    • panel becomes transparent if no window is touching it.
    • Transparency adjustable on the stylesheet

## Install from zip

• [Download the zip file](/lab/gse-rnb-panel/latest/rnb-panel.zip) and unzip it.
• copy the folder ``rnb-panel@omacronides.com`` into ``~/.local/share/gnome-shell/extensions/``.

## Install from source

°°stx-bash°°
    # Download sources
    git clone https://framagit.org/rui.nibau/gse-rnb-panel.git
    
    # Go to directory
    cd gse-rnb-panel
    
    # Checkout version to install
    git checkout tags/<version>
    
    # Install the extension
    ./project.sh install

## Ressources et références

• [Développement javascript sous Gnome](/articles/gjs)

## History

..include::./changelog.md

## Licence

..include::./licence.md

