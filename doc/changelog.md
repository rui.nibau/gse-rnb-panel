---
title:      Historique
date:       2023-09-25
updated:    2024-05-01
---

°°changelog°°
47.0.0 øø(2024-11-01)øø:
    • upd: Gnome 47 compatibility
46.0.0 øø(2024-05-01)øø:
    • fix: [Gnome 46](https://gjs.guide/extensions/upgrading/gnome-shell-46.html)
45.0.0 øø(2023-09-25)øø:
    • upd: [Gnome 45](https://gjs.guide/extensions/upgrading/gnome-shell-45.html)
    • upd: Merge with rnb-workspace and rnb-taskbar
    • add: preferences
1.0.0 øø(2023-01-15)øø:
    • add: First publication

