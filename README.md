# rnb-panel

Main Panel tweaks for Gnome ≥ 45.

• [Website](https://omacronides.com/projets/gse-rnb-panel/)
• [Source](https://framagit.org/rui.nibau/gse-rnb-panel)
• [Issues](https://framagit.org/rui.nibau/gse-rnb-panel/-/issues)

