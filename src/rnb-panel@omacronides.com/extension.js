import { Extension } from 'resource:///org/gnome/shell/extensions/extension.js';

import { panel } from 'resource:///org/gnome/shell/ui/main.js';

import ScrollManager from './libs/ScrollManager.js';
import TransparencyManager from './libs/TransparencyManager.js';
import AppsBar from './libs/AppsBar.js';
import AppsGridButton from './libs/AppsGridButton.js';


export default class TopBar extends Extension {
    /**
     * @type {TransparencyManager}
     */
    #transparency = null;

    /**
     * @type {ScrollManager}
     */
    #scroll = null;

    #appsBar = null;

    #appsGrid = null;

    #settings = null;

    enable() {
        console.log('Enable rnb-topbar');
        this.#settings = this.getSettings('org.gnome.shell.extensions.rnb-panel');
        this.#settings.connect('changed::apps-bar', (settings, key) => {
            this.activateAppsBar(settings.get_boolean(key));
        });
        this.activateAppsBar(this.#settings.get_boolean('apps-bar'));

        this.#settings.connect('changed::apps-grid', (settings, key) => {
            this.activateAppsGrid(settings.get_boolean(key));
        });
        this.activateAppsGrid(this.#settings.get_boolean('apps-grid'));

        this.#settings.connect('changed::scroll', (settings, key) => {
            this.activateScroll(settings.get_boolean(key));
        });
        this.activateScroll(this.#settings.get_boolean('scroll'));

        this.#settings.connect('changed::transparency', (settings, key) => {
            this.activateTransparency(settings.get_boolean(key));
        });
        this.activateTransparency(this.#settings.get_boolean('transparency'));
    }

    disable() {
        console.log('Disable rnb-topbar');
        this.activateAppsBar(false);
        this.activateAppsGrid(false);
        this.activateScroll(false);
        this.activateTransparency(false);
        this.#settings = null;
    }

    /**
     *
     * @param {boolean} state If Applications bar needs to be activated
     */
    activateAppsBar(state) {
        if (state) {
            if (!this.#appsBar) {
                this.#appsBar = new AppsBar();
                panel.addToStatusArea('rnb-apps-bar', this.#appsBar, this.#appsGrid ? 1 : 0, 'left');
            }
        } else {
            this.#appsBar?.destroy();
            this.#appsBar = null;
        }
    }

    /**
     *
     * @param {boolean} state If Applications grid button needs to be activated
     */
    activateAppsGrid(state) {
        if (state) {
            if (!this.#appsGrid) {
                this.#appsGrid = new AppsGridButton();
                panel.addToStatusArea('rnb-apps-grid', this.#appsGrid, 0, 'left');
            }
        } else {
            this.#appsGrid?.destroy();
            this.#appsGrid = null;
        }
    }

    /**
     * @param {boolean} state If scroll needs to be activated
     */
    activateScroll(state) {
        if (state) {
            if (!this.#scroll) {
                this.#scroll = new ScrollManager();
                this.#scroll.enable();
            }
        } else {
            this.#scroll?.disable();
            this.#scroll = null;
        }
    }

    /**
     * @param {boolean} state If transparency needs to be activated
     */
    activateTransparency(state) {
        if (state) {
            if (!this.#transparency) {
                this.#transparency = new TransparencyManager();
                this.#transparency.enable();
            }
        } else {
            this.#transparency?.disable();
            this.#transparency = null;
        }
    }
}
