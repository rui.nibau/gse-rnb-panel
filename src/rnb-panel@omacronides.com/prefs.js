import Gio from 'gi://Gio';
import Adw from 'gi://Adw';

import { ExtensionPreferences, gettext as _ } from 'resource:///org/gnome/Shell/Extensions/js/extensions/prefs.js';


export default class ExamplePreferences extends ExtensionPreferences {
    window = null;
    fillPreferencesWindow(window) {
        this.window = window;
        this.window._settings = this.getSettings('org.gnome.shell.extensions.rnb-panel');

        const page = new Adw.PreferencesPage({
            title: _('General'),
            icon_name: 'dialog-information-symbolic',
        });
        window.add(page);

        const group = new Adw.PreferencesGroup({
            title: _('Functionalities'),
            description: _('Activate functionalities'),
        });
        page.add(group);

        group.add(this.createBooleanProp('apps-bar', 'Apps Bar', 'Add applications bar'));
        group.add(this.createBooleanProp('apps-grid', 'Apps Grid', 'Add applications grid button'));
        group.add(this.createBooleanProp('scroll', 'Scroll', 'Switch workspace scrolling the panel'));
        group.add(this.createBooleanProp('transparency', 'Transparency', 'Activate panel transparency'));
    }

    /**
     *
     * @param {string} name Prop name
     * @param {string} title Prop title
     * @param {string} subtitle Prop subtitle
     * @returns {Adw.SwitchRow}
     */
    createBooleanProp(name, title, subtitle) {
        const row = new Adw.SwitchRow({ title: _(title), subtitle: _(subtitle) });
        this.window._settings.bind(name, row, 'active', Gio.SettingsBindFlags.DEFAULT);
        return row;
    }
}
