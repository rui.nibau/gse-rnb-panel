import Clutter from 'gi://Clutter';
import GObject from 'gi://GObject';
import Graphene from 'gi://Graphene';
import Meta from 'gi://Meta';
import Shell from 'gi://Shell';
import St from 'gi://St';

import { getAppFavorites } from 'resource:///org/gnome/shell/ui/appFavorites.js';
import { AppMenu } from 'resource:///org/gnome/shell/ui/appMenu.js';
import { zoomOutActor } from 'resource:///org/gnome/shell/ui/iconGrid.js';
import { Button } from 'resource:///org/gnome/shell/ui/panelMenu.js';
import { PopupMenuManager } from 'resource:///org/gnome/shell/ui/popupMenu.js';
import { activateWindow, uiGroup } from 'resource:///org/gnome/shell/ui/main.js';
import { disconnect, hideOverview, isClutterContainerRemoved } from '../utils.js';

const APP_GRID_ICON_NAME = 'view-app-grid-symbolic';
const ICON_SIZE = 22;
const SEP = '|';

const CLS_RUNNING = 'running';
const CLS_ACTIVE = 'active';

const SID = 'sizeChangedSignalId';


const createSeparator = () => {
    return new St.Widget({
        style_class: 'rnb-separator',
        x_align: Clutter.ActorAlign.FILL,
        y_align: Clutter.ActorAlign.CENTER,
        width: 1,
        height: 15,
    });
};

/**
 * @param {import('gi').Meta.Window} win
 * @param {number} index
 */
const openInWorkspace = (win, index) => {
    win.change_workspace_by_index(index, true);
    global.workspace_manager.get_workspace_by_index(index).activate(global.get_current_time());
};

/**
 * Indicates how many windows are opened for a given application
 */
class AppWinIndicator extends St.Widget {
    static {
        GObject.registerClass(this);
    }

    _init() {
        super._init({
            styleClass: 'rnb-app-win-indicator',
            layout_manager: new Clutter.BinLayout(),
            x_expand: true,
            y_expand: true,
            y_align: Clutter.ActorAlign.START,
        });
        /**
         * @private
         */
        this._value = 0;
    }

    get value() {
        return this._value;
    }

    /**
     * @param {number} value Number of windows opened
     */
    set value(value) {
        if (value !== this._value) {
            this.destroy_all_children();
            this._value = value > 4 ? 4 : value;
            for (let i = 1; i <= this._value; i += 1) {
                this.add_child(new St.Widget({
                    styleClass: 'dash',
                    x_expand: true,
                    y_expand: true,
                }));
            }
        }
    }

    vfunc_allocate(box) {
        super.vfunc_allocate(box);

        const children = this.get_children();
        if (children.length === 0) {
            return;
        }

        const actorBox = new Clutter.ActorBox();
        const contentBox = this.get_theme_node().get_content_box(box);
        const fullHeight = contentBox.y2 - contentBox.y1;
        const width = ((contentBox.x2 - contentBox.x1) / children.length) - 1;
        let x1 = contentBox.x1;
        for (let i = 0; i < children.length; i++) {
            const [, natHeight] = children[i].get_preferred_height(fullHeight);
            actorBox.x1 = x1;
            actorBox.x2 = actorBox.x1 + width;
            actorBox.y1 = contentBox.y1;
            actorBox.y2 = actorBox.y1 + natHeight;
            children[i].allocate(actorBox);
            x1 = actorBox.x2 + 2;
        }
    }
}


/**
 * Button for an application
 */
class AppButton extends St.Button {
    static {
        GObject.registerClass(this);
    }

    /**
     * @param {import('gi').Shell.App} app App
     */
    _init(app) {
        super._init({
            pivot_point: new Graphene.Point({ x: 0.5, y: 0.5 }),
            reactive: true,
            can_focus: true,
            track_hover: true,
        });

        this.add_style_class_name('rnb-app-button');

        /** @type {import('gi').Shell.App}*/
        this.app = app;

        this._menu = null;
        this._menuManager = new PopupMenuManager(this);

        this._indicator = new AppWinIndicator();

        const icon = new St.Bin({
            styleClass: 'rnb-app-icon',
            reactive: true,
            x_align: Clutter.ActorAlign.CENTER,
            y_expand: true,
            y_align: Clutter.ActorAlign.FILL,
            child: app.create_icon_texture(ICON_SIZE),
        });

        const group = new Clutter.Actor({
            layout_manager: new Clutter.BinLayout(),
            y_expand: true,
            y_align: Clutter.ActorAlign.FILL,
        });

        if (isClutterContainerRemoved) {
            group.add_child(icon);
            group.add_child(this._indicator);
        } else {
            group.add_actor(icon);
            group.add_actor(this._indicator);
        }

        this.set_child(group);

        this._connections = new Map([
            [this, [
                this.connect('button-release-event', this._on_button_event.bind(this)),
                this.connect('destroy', () => this.destroy()),
            ]],
            [app, [
                app.connect('windows-changed', () => this.update()),
            ]],
            [global.display, [
                global.display.connect('notify::focus-window', () => this._update_active_state()),
            ]],
        ]);

        this.update();
    }

    destroy() {
        this._menu?.close();
        this._connections.forEach((ids, object) => ids.forEach(id => object.disconnect(id)));
        this._connections.clear();
        this._menuManager = null;
        this._indicator = null;
        this.app = null;
    }

    /**
     *
     * @param {import('gi').Clutter.Actor} actor Actor
     * @param {import('gi').Clutter.Event} event Button event
     */
    _on_button_event(actor, event) {
        if (event.get_button() === 1) {
            this.activate();
        } else if (event.get_button() === 3) {
            this.popupMenu();
            return Clutter.EVENT_STOP;
        }
    }

    activate() {
        if (this.is_running()) {
            /** @type {import('gi').Meta.Window[]}*/
            const windows = this.get_real_windows();
            const index = windows.findIndex(window => window.has_focus());
            if (index > -1) {
                // active : switch window
                if (windows.length === 1) {
                    // minimize / restore
                    const window = windows[0];
                    if (window.can_minimize()) {
                        window.minimize();
                    } else {
                        window.raise();
                    }
                } else {
                    // cycle
                    const nextIndex = index === windows.length - 1 ? 0 : index + 1;
                    activateWindow(windows[nextIndex]);
                }
            } else {
                // activate
                this.app.activate();
            }
        } else {
            // run
            this.animateLaunch();
            this.app.activate();
            hideOverview();
        }
    }

    animateLaunch() {
        zoomOutActor(this.get_child());
    }

    popupMenu(side = St.Side.BOTTOM) {
        if (!this._menu) {
            this._menu = new AppMenu(this, side, {
                favoritesSection: true,
                showSingleWindows: true,
            });
            this._menu.blockSourceEvents = true;
            this._menu.setApp(this.app);
            if (isClutterContainerRemoved) {
                uiGroup.add_child(this._menu.actor);
            } else {
                uiGroup.add_actor(this._menu.actor);
            }
            this._menuManager.addMenu(this._menu);
        }
        this._menu.open();
        this._menuManager.ignoreRelease();
    }

    get_real_windows() {
        return this.app.get_windows().filter((/** @type {import('gi').Meta.Window}*/window) => {
            const type = window.get_window_type();
            return type === Meta.WindowType.DESKTOP || type === Meta.WindowType.NORMAL;
        });
    }

    /**
     * If app is running
     *
     * @returns {boolean}
     */
    is_running() {
        return this.get_real_windows().length > 0;
    }

    /**
     * Is the application active (with focus on a window) ?
     *
     * @returns {boolean}
     */
    is_active() {
        return this.app.get_windows().findIndex(window => window.has_focus()) !== -1;
    }

    update() {
        this._update_running_state();
        this._update_active_state();
        this._update_indicators();
    }

    /**
     * @private
     */
    _update_running_state() {
        if (this.is_running()) {
            this.add_style_class_name('running');
        } else {
            this.remove_style_class_name('running');
        }
    }

    /**
     * @private
     */
    _update_active_state() {
        if (this.is_active()) {
            this.add_style_class_name('active');
        } else {
            this.remove_style_class_name('active');
        }
    }

    /**
     * @private
     */
    _update_indicators() {
        if (this._indicator) {
            this._indicator.value = this.get_real_windows().length;
        }
    }
}

/**
 * Taskbar. It shoes favorites and running applications.
 */
export default class AppsBar extends Button {
    static {
        GObject.registerClass(this);
    }

    // XXX We can't declare members like that here !
    // _bar = null;
    // _connects = null;

    _init() {
        super._init();
        this.add_style_class_name('rnb-apps-bar');

        this._bar = new St.BoxLayout({});

        this.add_child(this._bar);

        const appSystem = Shell.AppSystem.get_default();
        const favorites = getAppFavorites();

        /** @type {Map.<object, number[]>}*/
        this._connects = new Map([
            [appSystem, [
                appSystem.connect('app-state-changed', (appsys, app) => {
                    const state = app.get_state();
                    if (state === Shell.AppState.RUNNING) {
                        this.add_app(app);
                    } else if (state === Shell.AppState.STOPPED) {
                        this.remove_app(app);
                    }
                }),
            ]],
            [favorites, [
                favorites.connect('changed', () => this.redraw()),
            ]],
        ]);

        // display
        this.redraw();
    }

    destroy() {
        console.log('AppsBar.destroy()');
        disconnect(this._connects);
        this._connects = null;
        this._bar.destroy_all_children();
        this._bar.destroy();
        this._bar = null;
        super.destroy();
    }

    /**
     * Move Maximized windows on the last workspace
     *
     * @private
     * @param {import('gi').Meta.Window} win Window
     */
    _onWindowStateChanged(win) {
        const wsManager = global.workspace_manager;
        /** @type {import('gi').Meta.Workspace} */
        const activeWs = wsManager.get_active_workspace();
        const currentIndex = wsManager.get_active_workspace_index();
        const windows = activeWs.list_windows().filter(w => w.is_on_primary_monitor());

        console.log(`Active workspace (${activeWs.index()}): ${activeWs.nWindows} windows`);
        console.log(`Window workspace  (${win.get_workspace().index()}): ${win.get_workspace().nWindows} windows`);
        const wsIndex = windows.length === 0 ? currentIndex : wsManager.n_workspaces;
        if (win.get_maximized() === Meta.MaximizeFlags.BOTH && win.get_workspace().nWindows > 0) {
            if (global.display.get_current_monitor() === global.display.get_primary_monitor()) {
                openInWorkspace(win, wsIndex);
                // win.change_workspace_by_index(wsIndex, true);
                // wsManager.get_workspace_by_index(wsIndex).activate(global.get_current_time());
            }
        }
        if (win[SID]) {
            win.disconnect(win[SID]);
            delete win[SID];
        }
    }

    /**
     * @param {import('gi').Shell.App} app Application to add
     */
    add_app(app) {
        // Append/update button
        console.log(`add app: ${app.get_id()} - ${app.get_name()}`);
        const id = app.get_id();
        /** @type {AppButton} */
        const button = this._bar.get_children().find(c => c.app && c.app.get_id() === id);
        if (button) {
            button.update();
        } else {
            this._bar.add_child(new AppButton(app));
        }
        const wins = app.get_windows();
        const win = wins[wins.length - 1];
        if (wins.length > 1) {
            const ws = /** @type {import('gi').Meta.Workspace}*/wins[0].get_workspace();
            openInWorkspace(win, ws.index());
        } else if (!win[SID]) {
            win[SID] = win.connect('size-changed', this._onWindowStateChanged);
        }
    }

    /**
     * @param {import('gi://Shell').default.App} app Application to remove
     */
    remove_app(app) {
        const id = app.get_id();
        console.log(`remove app: ${id}`);
        const i = this._bar.get_children().findIndex(c => c.app && c.app.get_id() === id);
        if (i !== undefined && i > -1) {
            /** @type {AppButton} */
            const button = this._bar.get_child_at_index(i);
            if (button) {
                if (!getAppFavorites().isFavorite(id)) {
                    // favorites
                    button.destroy();
                    this._bar.remove_child(button);
                } else {
                    button.update();
                }
            }
        }
    }

    /**
     * Shell.AppSystem.get_default().get_running() is slow to update
     * use this function from Dash to Panel instead.
     * Took from https://gitlab.com/AndrewZaech/aztaskbar/-/raw/main/extension.js
     *
     * @returns {App}
     */
    _getRunning() {
        const tracker = Shell.WindowTracker.get_default();
        const windows = global.get_window_actors();
        const appFavorites = getAppFavorites();
        const apps = [];
        for (let i = 0, l = windows.length; i < l; ++i) {
            const app = tracker.get_window_app(windows[i].metaWindow);
            if (app && apps.indexOf(app) < 0 && !appFavorites.isFavorite(app.get_id())) {
                apps.push(app);
            }
        }
        return apps;
    }

    redraw() {
        console.log('AppsBar.redraw()');
        // clear
        this._bar.destroy_all_children();

        const appFavorites = getAppFavorites();
        const favorites = appFavorites.getFavorites();
        const running = Shell.AppSystem.get_default().get_running().filter(app => {
            return !appFavorites.isFavorite(app.get_id());
        });
        /**
         * @type {(import('gi').Shell.App|string)[]}
         */
        [...favorites, SEP, ...running].forEach(app => {
            if (typeof app === 'string') {
                this._bar.add_child(createSeparator());
            } else {
                this._bar.add_child(new AppButton(app));
            }
        });
    }
}
