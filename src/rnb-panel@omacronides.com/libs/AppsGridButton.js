import GObject from 'gi://GObject';
import St from 'gi://St';

import { Button } from 'resource:///org/gnome/shell/ui/panelMenu.js';
import { overview } from 'resource:///org/gnome/shell/ui/main.js';
import { hideOverview } from '../utils.js';

const APP_GRID_ICON_NAME = 'view-app-grid-symbolic';

export default class AppsGridButton extends Button {
    static {
        GObject.registerClass(this);
    }

    _init() {
        super._init(0.0, 'App grid button');
        this.add_style_class_name('rnb-apps-grid');

        this.box = new St.BoxLayout({
            visible: true,
            reactive: true,
            can_focus: false,
            track_hover: false,
        });
        this.box.add_child(new St.Icon({
            icon_name: APP_GRID_ICON_NAME,
            style_class: 'system-status-icon',
        }));
        this._connectId = this.box.connect('button-release-event', () => {
            if (!hideOverview()) {
                this.add_style_class_name('running');
                overview.showApps();
            } else {
                this.remove_style_class_name('running');
            }
        });
        this.add_child(this.box);
    }

    destroy() {
        const box = this.get_children()[0];
        box.disconnect(this._connectId);
        box.destroy();
        this._connectId = null;
        super.destroy();
    }
}
