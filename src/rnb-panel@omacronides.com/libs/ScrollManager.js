import Clutter from 'gi://Clutter';
import { panel } from 'resource:///org/gnome/shell/ui/main.js';
import { disconnect } from '../utils.js';

export default class ScrollManager {
    /** @type {Map<object, number[]>} */
    #connects = null;

    enable() {
        console.log('scrollManager.enable()');
        this.#connects = new Map();
        this.#connects.set(panel, [
            panel.connect_after('scroll-event', this._onScroll.bind(this)),
        ]);
    }

    disable() {
        console.log('scrollManager.disable()');
        disconnect(this.#connects);
        this.#connects = null;
    }

    /**
     * @param {import('gi').Clutter.Actor} actor Where the scroll happens
     * @param {import('gi').Clutter.Event} event Event
     * @returns {number}
     */
    _onScroll(actor, event) {
        // const source = event.get_source();
        // FIXME On gnome 45 : source is null
        // // scroll on right box : just propagate event
        // if (source !== actor && panel._rightBox && panel._rightBox.contains && panel._rightBox.contains(source)) {
        //     return Clutter.EVENT_PROPAGATE;
        // }
        // Scroll to workspace
        let diff = 0;
        switch (event.get_scroll_direction()) {
        case Clutter.ScrollDirection.DOWN:
        case Clutter.ScrollDirection.RIGHT:
            diff = 1;
            break;
        case Clutter.ScrollDirection.UP:
        case Clutter.ScrollDirection.LEFT:
            diff = -1;
            break;
        }
        if (diff !== 0) {
            this.activate(global.workspace_manager.get_active_workspace_index() + diff);
        }
        return Clutter.EVENT_STOP;
    }

    /**
     * Activate a workspace
     *
     * @param {number} index of the workspace
     */
    activate(index) {
        if (index >= 0 && index < global.workspace_manager.n_workspaces) {
            console.log(`scrollManager.activate(${index})`);
            const currentWs = global.workspace_manager.get_active_workspace();
            const newWs = global.workspace_manager.get_workspace_by_index(index);
            if (newWs !== currentWs) {
                newWs.activate(global.get_current_time());
            }
        }
    }
}
