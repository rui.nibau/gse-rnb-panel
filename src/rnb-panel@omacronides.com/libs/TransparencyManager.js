import { layoutManager, overview, panel, sessionMode } from 'resource:///org/gnome/shell/ui/main.js';
import { disconnect, isClutterContainerRemoved, windowNearTopbar } from '../utils.js';

const CLS = 'rnb-panel';
const CLS_TRANS = 'rnb-panel-trans';

export default class TransparencyManager {
    /** @type {Map<object, number[]>} */
    #connects = null;

    /** @type {(...args: any[]) => void} */
    #updateListener = null;

    enable() {
        console.log('TransparencyManager.enable()');
        this.#connects = new Map();
        this.#updateListener = this.#update.bind(this);

        this.#connects.set(overview, [
            overview.connect('showing', this.#updateListener),
            overview.connect('hiding', this.#updateListener),
        ]);

        this.#connects.set(sessionMode, [
            sessionMode.connect('updated', this.#updateListener),
        ]);
        for (const metaWindowActor of global.get_window_actors()) {
            this._onWindowActorAdded(metaWindowActor.get_parent(), metaWindowActor);
        }
        if (isClutterContainerRemoved) {
            this.#connects.set(global.window_group, [
                global.window_group.connect('child-added', this._onWindowActorAdded.bind(this)),
                global.window_group.connect('child-removed', this._onWindowActorRemoved.bind(this)),
            ]);
        } else {
            this.#connects.set(global.window_group, [
                global.window_group.connect('actor-added', this._onWindowActorAdded.bind(this)),
                global.window_group.connect('actor-removed', this._onWindowActorRemoved.bind(this)),
            ]);
        }

        this.#connects.set(global.window_manager, [
            global.window_manager.connect('switch-workspace', this.#updateListener),
        ]);

        panel.add_style_class_name(CLS);

        this.#update();
    }

    disable() {
        console.log('TransparencyManager.disable()');
        panel.remove_style_class_name(CLS);
        panel.remove_style_class_name(CLS_TRANS);
        disconnect(this.#connects);
        this.#connects = null;
        this.#updateListener = null;
    }

    /**
     *
     * @param {any} container Container
     * @param {import('gi').Meta.WindowActor} metaWindowActor Actor
     */
    _onWindowActorAdded(container, metaWindowActor) {
        this.#connects.set(metaWindowActor, [
            metaWindowActor.connect('notify::allocation', this.#updateListener),
            metaWindowActor.connect('notify::visible', this.#updateListener),
        ]);
    }

    /**
     *
     * @param {any} container Container
     * @param {import('gi').Meta.WindowActor} metaWindowActor Actor
     */
    _onWindowActorRemoved(container, metaWindowActor) {
        this.#connects.get(metaWindowActor)?.forEach(id => metaWindowActor.disconnect(id));
        this.#connects.delete(metaWindowActor);
        this.#update();
    }

    #update() {
        if (panel.has_style_pseudo_class('overview') || !sessionMode.hasWindows) {
            this.transparent = true;
            return;
        }
        if (!layoutManager.primaryMonitor) {
            return;
        }

        this.transparent = !windowNearTopbar();
    }

    /**
     * @param {boolean} newValue Transparency
     */
    set transparent(newValue) {
        if (newValue) {
            panel.add_style_class_name(CLS_TRANS);
        } else {
            panel.remove_style_class_name(CLS_TRANS);
        }
    }

    get transparent() {
        return panel.has_style_class_name(CLS_TRANS);
    }
}
