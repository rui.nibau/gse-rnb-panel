import Clutter from 'gi://Clutter';
import Meta from 'gi://Meta';
import St from 'gi://St';
import { overview, panel } from 'resource:///org/gnome/shell/ui/main.js';


/**
 * If a window is near the topbar ?
 *
 * @returns {boolean}
 */
export const windowNearTopbar = () => {
    // Get all the windows in the active workspace that are in the primary monitor and visible.
    const workspaceManager = global.workspace_manager;
    const activeWorkspace = workspaceManager.get_active_workspace();
    const windows = activeWorkspace.list_windows().filter((/** @type {import('gi').Meta.Window}*/metaWindow) => {
        return metaWindow.is_on_primary_monitor() &&
           metaWindow.showing_on_its_workspace()  &&
           !metaWindow.is_hidden() &&
           metaWindow.get_window_type() !== Meta.WindowType.DESKTOP;
    });

    // Check if at least one window is near enough to the panel.
    const panelTop = panel.get_transformed_position()[1];
    const panelBottom = panelTop + panel.get_height();
    const scale = St.ThemeContext.get_for_stage(global.stage).scale_factor;

    return windows.some((/** @type {import('gi').Meta.Window}*/metaWindow) => {
        return metaWindow.get_frame_rect().y < panelBottom + 5 * scale;
    });
};

/**
 * Disconnect listeners
 *
 * @param {Map<object, number[]>} map Map of object listeners ids
 */
export const disconnect = map => {
    map?.forEach((ids, source) => ids.forEach(id => source.disconnect(id)));
    map?.clear();
};

/**
 * Hide overview if necessary
 *
 * @returns {boolean} If overview has been hidden
 */
export const hideOverview = () => {
    if (overview.visible) {
        overview.hide();
        return true;
    }
    return false;
};

/**
 * Clutter.container removed in Gnome 46
 *
 * @type {boolean}
 */
export const isClutterContainerRemoved = Clutter.Container === undefined;
